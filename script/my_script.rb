require 'addressable/uri'
require 'rest-client'

url = Addressable::URI.new(
  scheme: 'http',
  host: 'localhost',
  port: 3000,
  path: '/users/1/contacts.json'
  #query_values: {"name" => "logan2", "email" => "logan2@logan2.logan2"}
).to_s

puts RestClient.get(url)#, {contact_share: {user_id: 1, contact_id: 2}})


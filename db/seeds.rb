# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

logan = User.create( name: "logan", email: "logan@logan.logan")
abe =  User.create( name: "abe", email: "abe@abe.abe")
abe2 =  User.create( name: "abe2", email: "abe2@abe.abe")
abe3 =  User.create( name: "abe3", email: "abe3@abe.abe")
abe4 =  User.create( name: "abe4", email: "abe4@abe.abe")
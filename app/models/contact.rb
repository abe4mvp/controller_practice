class Contact < ActiveRecord::Base
  attr_accessible :name, :email, :user_id

  validates :name, :email, :user_id, presence: :true

  belongs_to(
  :user,
  :class_name => "User",
  :foreign_key => :user_id,
  :primary_key => :id
  )


end
